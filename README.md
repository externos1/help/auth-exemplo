# Porto Itapoá - Auth-Exemplo

Projeto de exemplo para configuração de segurança com JWT

Neste projeto estão todas as configurações para validaçao do token, 
com as claims.

### Pacotes Importantes

Nestes pacotes estão as configurações principais de segurança.

- com.portoitapoa.authexemplo.property
  - Informações base do token, como data de expiração e chave de verificação
- com.portoitapoa.authexemplo.security.config
  - Configurações dos filtros para as credenciais
- com.portoitapoa.authexemplo.token.security
  - Autorização e validação do token

### Retornando Informações Usuario Auntenticado

Uma vez o token validado, as informações do usuário que foi representado na assinatura do token,
pode ser resgatado atravéz do SecurityContextHolder, que retorna um Principal. Exemplo:
-  ```(Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal()```


# Ambiente Para Desenvolvimento

## Pré requisito
- [Spring tool Suite 4](https://spring.io/tools/)
- Maven 3
- Java 11
- Docker 18+

# Usando Exemplo Para Testar Autenticação

## Tokens Para Testes

Foi disponibilizado dois tokens para testes, os mesmos podem ser encontrados no arquivo ./tokens_testes.txt

## EndPoints Testes

Esta API de exemplo contem dois serviços, onde cada serviço tem um nível de acesso

### [GET] /v1/configuracoes

Este serviço do tipo GET retorna um exemplo das configurações, e foi configurado com nível de acesso baixo.
O mesmo pode ser executado com os dois tokens (USER e ADMIN)

### [POST] /v1/configuracoes

Este serviço do tipo POST altera um exemplo das configurações, e foi configurado com nível de acesso acima.
O mesmo pode ser executado somente com o token ADMIN

### Configurações de níveis de acesso

As configurações de níveis de acesso exemplificada acima, podem ser encontradas no pacote com.portoitapoa.authexemplo.endpoint.controller

# Executando API de Exemplo

Esta API utiliza Swagger para representar a documentação, e realizar seus testes de apoio.

### Executando Localmente
A mesma pode ser executada localmente caso o sistema esteja de acordo com os pré requisitos de ambiente (listados acima)

1. Clonar o Projeto
2. Realizar um Clean com Maven
3. Executar utilizando STS 4
  1. Clicar com botão direito na classe AuthExemploApplication.java > Runn Ass > Spring Boot App

### Executando com Docker

```docker container run -d -p 2196:2196 --name my_exemple portoitapoa/auth-exemplo:latest```

### Url de acesso
- http://localhost:2196/swagger-ui.html

## Links

- http://localhost:2196/swagger-ui.html
- https://hub.docker.com/repository/docker/portoitapoa/auth-exemplo





  


