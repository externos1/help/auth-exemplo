package com.portoitapoa.authexemplo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Fernando de Lima
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ConfiguracoesVO {

	@ApiModelProperty(example = "10", required = false, value = "Quantidade mínima de acessos", position=1)
	private Integer qtdMinAcessos;

	@ApiModelProperty(example = "10", required = false, value = "Quantidade máxima de acessos", position=2)
	private Integer qtdMaxAcessos;
	
	@ApiModelProperty(example = "fernando.lima", required = false, value = "Usuário responsável pela alteração das configurações", position=3)
	private String usuario;
	
	@ApiModelProperty(example = "Configurações ativas", required = false, value = "Descrição das configurações", position=4)
	private String descricao;
}
