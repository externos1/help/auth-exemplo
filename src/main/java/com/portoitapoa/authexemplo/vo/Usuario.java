package com.portoitapoa.authexemplo.vo;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	private String codUsuario;

	private String senha;

	private String nomUsuario;

	private String cpf;

	private Boolean logAdmin;

	private Date datUltimoLogin;

	private Date ativoAte;

	private String email;

	@Builder.Default
	private String roles = "USER";

	private Boolean alterarSenha;

	private Integer tokenLevel;

	public boolean getAtivo() {
		if (ativoAte != null && ativoAte.before(new Date())) {
			return false;
		}
		return true;
	}

	public Usuario(@NotNull Usuario applicationUser) {
		this.nomUsuario = applicationUser.getNomUsuario();
		this.logAdmin = applicationUser.getLogAdmin();
		this.datUltimoLogin = applicationUser.getDatUltimoLogin();
		this.ativoAte = applicationUser.getAtivoAte();
		this.alterarSenha = applicationUser.getAlterarSenha();
		
		this.cpf = applicationUser.getCpf();
		this.codUsuario = applicationUser.getCodUsuario();
		this.senha = applicationUser.getSenha();
		this.roles = applicationUser.getRoles();
		
		this.email = applicationUser.getEmail();
		
		this.tokenLevel = applicationUser.getTokenLevel();
	}
}