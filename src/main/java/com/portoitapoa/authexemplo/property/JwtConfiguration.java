/**
 * 
 */
package com.portoitapoa.authexemplo.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Fernando de Lima
 *
 */
@Configuration
@ConfigurationProperties(prefix = "jwt.config")
@Getter
@Setter
@ToString
public class JwtConfiguration {
    private String loginUrl = "/login/**";
    @NestedConfigurationProperty
    private Header header = new Header();
    private int expirationLevel1 = 7200; //2 horas <= (2 * (60 * 60)) segundos
    private int expirationLevel2 = 604800; //7 dias <= 7 * (24 * (60 * 60)) segundos
    private int expirationLevel3 = 31536000; //1 anoe <= 365 * (24 * (60 * 60)) segundos
    private String privateKey = "Q7NffcwW8UcDdtqtMpK5JD6Qij470MjJ";
    private String type = "encrypted";

    @Getter
    @Setter
    public static class Header {
        private String name = "Authorization";
        private String prefix = "Bearer ";
    }
}