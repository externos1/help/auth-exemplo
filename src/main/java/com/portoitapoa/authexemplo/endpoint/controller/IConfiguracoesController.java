package com.portoitapoa.authexemplo.endpoint.controller;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.portoitapoa.authexemplo.vo.ConfiguracoesVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author Fernando de Lima
 *
 */
@RestController
@RequestMapping("/v1/configuracoes")
@CrossOrigin(origins = "*")
@Api(value = "", tags = {"Configurações"},  description = "Serviços de gerenciamento das Configurações gerais do Sistema") 
public interface IConfiguracoesController {
	
	@ApiOperation(value = "Altera Configurações",  nickname = "alteraConfiguracoes", notes = "Altera dados das configurações gerais")
	@PutMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Secured("ROLE_AUTH_EXEMPLO_ADMIN")
	ResponseEntity<ConfiguracoesVO> putConfig(@Valid @RequestBody ConfiguracoesVO configVO, @RequestHeader("Authorization") String authorization);

	@ApiOperation(value = "Consulta Configurações",  nickname = "consultaConfiguracoes", notes = "Consulta dados das configurações gerais")
	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Secured("ROLE_AUTH_EXEMPLO_USER")
	ResponseEntity<ConfiguracoesVO> getConfig(@RequestHeader("Authorization") String authorization);
}
