/**
 * 
 */
package com.portoitapoa.authexemplo.endpoint.controller.imp;

import java.text.ParseException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.portoitapoa.authexemplo.endpoint.controller.IConfiguracoesController;
import com.portoitapoa.authexemplo.error.WSAuthExemploException;
import com.portoitapoa.authexemplo.service.ConfiguracoesService;
import com.portoitapoa.authexemplo.util.Util;
import com.portoitapoa.authexemplo.vo.ConfiguracoesVO;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Fernando de Lima
 *
 */
@Component
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ConfiguracoesController implements IConfiguracoesController {
	
	private final ConfiguracoesService configSvc;
	
	@Override
	public ResponseEntity<ConfiguracoesVO> putConfig(@Valid ConfiguracoesVO configVO, String authorization) {

		log.info("{} Alterando configurações gerais", Util.LOG_PREFIX);

		try {
			ConfiguracoesVO configNovo = this.configSvc.alterarConfig(configVO);
			
			return ResponseEntity.ok(configNovo);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new WSAuthExemploException(e.getMessage());
		}
	}

	@Override
	public ResponseEntity<ConfiguracoesVO> getConfig(String authorization) {

		log.info("{} Alterando configurações gerais", Util.LOG_PREFIX);

		return ResponseEntity.ok(this.configSvc.findConfigGerais());
	}
}
