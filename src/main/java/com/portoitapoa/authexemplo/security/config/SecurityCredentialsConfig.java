/**
 * 
 */
package com.portoitapoa.authexemplo.security.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.portoitapoa.authexemplo.property.JwtConfiguration;
import com.portoitapoa.authexemplo.token.security.JwtTokenAuthorizationFilter;
import com.portoitapoa.authexemplo.token.security.SecurityTokenConfig;
import com.portoitapoa.authexemplo.token.security.TokenConverter;

/**
 * @author Fernando de Lima
 *
 */
@EnableWebSecurity
public class SecurityCredentialsConfig extends SecurityTokenConfig {
    private final TokenConverter tokenConverter;

    public SecurityCredentialsConfig(JwtConfiguration jwtConfiguration, TokenConverter tokenConverter) {
        super(jwtConfiguration);
        this.tokenConverter = tokenConverter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.addFilterAfter(new JwtTokenAuthorizationFilter(jwtConfiguration, tokenConverter), UsernamePasswordAuthenticationFilter.class);
        super.configure(http);
    }
}