package com.portoitapoa.authexemplo.service;

import java.text.ParseException;

import org.springframework.stereotype.Service;

import com.portoitapoa.authexemplo.util.Util;
import com.portoitapoa.authexemplo.vo.ConfiguracoesVO;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Fernando de Lima
 *
 */
@Service
@Slf4j
public class ConfiguracoesService {

	public ConfiguracoesVO alterarConfig(ConfiguracoesVO configVO) throws ParseException {
		
		log.info("{} Alterando configuracões gerais", Util.LOG_PREFIX);
		
		configVO.setUsuario(Util.getUsuarioCorrente().getCodUsuario());
		
		return configVO;
	}

	
	public ConfiguracoesVO findConfigGerais() {
		
		log.info("{} Buscando configurações gerais", Util.LOG_PREFIX);
		
		return ConfiguracoesVO.builder()
				.descricao("Nova Configuração")
				.qtdMinAcessos(10)
				.qtdMaxAcessos(500)
				.usuario(Util.getUsuarioCorrente().getCodUsuario())
				.build();
	}
}
