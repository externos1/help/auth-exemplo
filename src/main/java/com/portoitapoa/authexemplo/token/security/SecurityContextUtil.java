/**
 * 
 */
package com.portoitapoa.authexemplo.token.security;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.portoitapoa.authexemplo.util.Util;
import com.portoitapoa.authexemplo.vo.Usuario;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Fernando de Lima
 *
 */
@Slf4j
public class SecurityContextUtil {
    private SecurityContextUtil() {

    }

    public static void setSecurityContext(SignedJWT signedJWT) {
        try {
            JWTClaimsSet claims = signedJWT.getJWTClaimsSet();
            String cpf = claims.getSubject();
            if (cpf == null)
                throw new JOSEException(Util.LOG_PREFIX + " Nome de usuário ausente do JWT");

            List<String> authorities = claims.getStringListClaim("authorities");
            Usuario applicationUser = Usuario
                    .builder()
                    .codUsuario(claims.getStringClaim("userCodigo"))
                    .cpf(cpf)
                    .email(claims.getIssuer())
                    .roles(String.join(",", authorities))
                    .build();
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(applicationUser, null, createAuthorities(authorities));
            auth.setDetails(signedJWT.serialize());

            SecurityContextHolder.getContext().setAuthentication(auth);
        } catch (Exception e) {
            log.error(Util.LOG_PREFIX + " Erro ao definir contexto de segurança", e);
            SecurityContextHolder.clearContext();
        }
    }

    private static List<SimpleGrantedAuthority> createAuthorities(List<String> authorities) {
        return authorities.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(toList());
    }
}