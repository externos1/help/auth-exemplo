/**
 * 
 */
package com.portoitapoa.authexemplo.token.security;

import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.portoitapoa.authexemplo.property.JwtConfiguration;

import lombok.RequiredArgsConstructor;

/**
 * @author Fernando de Lima
 *
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SecurityTokenConfig extends WebSecurityConfigurerAdapter {
	
	protected final JwtConfiguration jwtConfiguration;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(STATELESS)
                .and()
                .exceptionHandling().authenticationEntryPoint((req, resp, e) -> resp.sendError(HttpServletResponse.SC_UNAUTHORIZED))
                .and()
                .authorizeRequests()
                .antMatchers(
                		HttpMethod.OPTIONS,
                		"/",
                		"/*.html",
                		"/favicon.ico",
                		"/**/*.html",
                		"/**/*.css",
                		"/**/*.js",
                		"**/swagger-resources/**" ,
                		"/**/swagger-ui.html" ,
                		"/v2/api-docs " ,
                		"/webjars/** ",
                		"/v2/api-docs",
                		"/swagger-resources", 
                		"/swagger-resources/configuration/ui", 
                		"/swagger-resources/configuration/security",
                		"/**/actuator/**").permitAll()
                .antMatchers(jwtConfiguration.getLoginUrl(), 
                		"/**/swagger-ui.html").permitAll()
                .antMatchers(HttpMethod.GET, 
                		"/**/actuator/**", 
                		"/**/swagger-resources/**", 
                		"/**/webjars/springfox-swagger-ui/**", 
                		"/**/v2/api-docs/**").permitAll()
                .antMatchers("/**/blc/v1/balance/PING").permitAll()
                .antMatchers("/**/usuario/alterar-senha").permitAll()
                .anyRequest().authenticated();
    }
}
