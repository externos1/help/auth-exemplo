/**
 * 
 */
package com.portoitapoa.authexemplo.token.security;

import static java.util.stream.Collectors.toList;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.RSAPublicKey;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.DirectEncrypter;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.portoitapoa.authexemplo.property.JwtConfiguration;
import com.portoitapoa.authexemplo.util.Util;
import com.portoitapoa.authexemplo.vo.Usuario;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Fernando de Lima
 *
 */
@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TokenCreator {
    private final JwtConfiguration jwtConfiguration;

    @SneakyThrows
    public SignedJWT createSignedJWT(Authentication auth) {
        log.info("{} Começando a criar o JWT assinado", Util.LOG_PREFIX);

        Usuario usuario = (Usuario) auth.getPrincipal();

        JWTClaimsSet jwtClaimSet = createJWTClaimSet(auth, usuario);

        KeyPair rsaKeys = generateKeyPair();

        log.info("{} Construindo o JWK a partir das chaves RSA", Util.LOG_PREFIX);

        JWK jwk = new RSAKey.Builder((RSAPublicKey) rsaKeys.getPublic()).keyID(UUID.randomUUID().toString()).build();

        SignedJWT signedJWT = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.RS256)
                .jwk(jwk)
                .type(JOSEObjectType.JWT)
                .build(), jwtClaimSet);

        log.info("{} Assinando o token com a chave privada RSA", Util.LOG_PREFIX);

        RSASSASigner signer = new RSASSASigner(rsaKeys.getPrivate());

        signedJWT.sign(signer);

        log.info(Util.LOG_PREFIX + " Serializando token '{}'", signedJWT.serialize());

        return signedJWT;

    }

    private JWTClaimsSet createJWTClaimSet(Authentication auth, Usuario usuario) {
        log.info(Util.LOG_PREFIX + " Criando o objeto JwtClaimSet para '{}'", usuario);

        return new JWTClaimsSet.Builder()
                .subject(usuario.getCpf())
                .claim("authorities", auth.getAuthorities()
                        .stream()
                        .map(GrantedAuthority::getAuthority)
                        .collect(toList()))
                .claim("userCodigo", usuario.getCodUsuario())
                .issuer(usuario.getEmail())
                .issueTime(new Date())
                .expirationTime(getDateExpiration(usuario.getTokenLevel()))
                .build();
    }

    private Date getDateExpiration(Integer tokenLevel) {
    	
    	Calendar calendarBr = Calendar.getInstance(new Locale("br"));
    	
    	switch (tokenLevel) {
		case 2:
			calendarBr.add(Calendar.SECOND, this.jwtConfiguration.getExpirationLevel2());
			return calendarBr.getTime();
		case 3:
			calendarBr.add(Calendar.SECOND, this.jwtConfiguration.getExpirationLevel3());
			return calendarBr.getTime();
		default:
			calendarBr.add(Calendar.SECOND, this.jwtConfiguration.getExpirationLevel1());
			return calendarBr.getTime();
		}
	}

	@SneakyThrows
    private KeyPair generateKeyPair() {
        log.info("{} Gerando RSA 2048 bits Keys", Util.LOG_PREFIX);

        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");

        generator.initialize(2048);

        return generator.genKeyPair();
    }


    public String encryptToken(SignedJWT signedJWT) throws JOSEException {
        log.info("{} Iniciando o método encryptToken", Util.LOG_PREFIX);

        DirectEncrypter directEncrypter = new DirectEncrypter(jwtConfiguration.getPrivateKey().getBytes());

        JWEObject jweObject = new JWEObject(new JWEHeader.Builder(JWEAlgorithm.DIR, EncryptionMethod.A128CBC_HS256)
                .contentType("JWT")
                .build(), new Payload(signedJWT));

        log.info("{} Criptografando token com chave privada do sistema", Util.LOG_PREFIX);

        jweObject.encrypt(directEncrypter);

        log.info("{} Token encrypted", Util.LOG_PREFIX);

        return jweObject.serialize();
    }
}