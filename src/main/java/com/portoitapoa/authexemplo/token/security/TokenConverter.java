/**
 * 
 */
package com.portoitapoa.authexemplo.token.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.crypto.DirectDecrypter;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.SignedJWT;
import com.portoitapoa.authexemplo.property.JwtConfiguration;
import com.portoitapoa.authexemplo.util.Util;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Fernando de Lima
 *
 */
@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TokenConverter {
    private final JwtConfiguration jwtConfiguration;

    @SneakyThrows
    public String decryptToken(String encryptedToken) {
        log.info("{} Decodificando o token", Util.LOG_PREFIX);

        JWEObject jweObject = JWEObject.parse(encryptedToken);

        DirectDecrypter directDecrypter = new DirectDecrypter(jwtConfiguration.getPrivateKey().getBytes());

        jweObject.decrypt(directDecrypter);

        log.info("{} Token descriptografado, retornando token assinado . . . ", Util.LOG_PREFIX);

        return jweObject.getPayload().toSignedJWT().serialize();
    }

    @SneakyThrows
    public void validateTokenSignature(String signedToken) throws AccessDeniedException {
        log.info("{} Iniciando o método para validar a assinatura do token...", Util.LOG_PREFIX);

        SignedJWT signedJWT = SignedJWT.parse(signedToken);

        log.info("{} Token Parsed! Recuperando Chave Pública do Token Assinado", Util.LOG_PREFIX);

        RSAKey publicKey = RSAKey.parse(signedJWT.getHeader().getJWK().toJSONObject());

        log.info("{} Chave pública recuperada, validando assinatura. . . ", Util.LOG_PREFIX);

        if (!signedJWT.verify(new RSASSAVerifier(publicKey))) {
        	throw new AccessDeniedException(Util.LOG_PREFIX + " Assinatura de token inválida!");
        }
        
        Date expires = signedJWT.getJWTClaimsSet().getExpirationTime();
        
        if (expires == null || new Date().after(expires)) {
        	log.info("{} Token expirado!", Util.LOG_PREFIX);
        	throw new AccessDeniedException(Util.LOG_PREFIX + " Token expirado!");
        }

        log.info("{} O token tem uma assinatura válida", Util.LOG_PREFIX);
    }
}