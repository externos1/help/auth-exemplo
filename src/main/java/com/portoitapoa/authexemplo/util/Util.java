package com.portoitapoa.authexemplo.util;

import java.time.ZoneId;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.portoitapoa.authexemplo.vo.Usuario;

import lombok.RequiredArgsConstructor;

/**
 * 
 * @author Fernando de Lima
 *
 */
@Configuration
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class Util {

	public static final ZoneId ZONA_ID = ZoneId.of("America/Sao_Paulo");
	public static final Locale LOCALE_PT_BR = new Locale("pt", "BR");
	
	public static String LOG_PREFIX;

	@Value("${server.undertow.accesslog.prefix}")
	public void setSvnUrl(String svnUrl) {
		LOG_PREFIX = svnUrl;
	}
	
	/**
	 * @return Informações do usuário logado
	 */
	public static Usuario getUsuarioCorrente() {
		return (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
}
