package com.portoitapoa.authexemplo.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Fernando de Lima
 *
 */
@ResponseStatus(code=HttpStatus.BAD_REQUEST)
public class WSAuthExemploException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public WSAuthExemploException(String mensagem) {
		super(mensagem);
	}

	public WSAuthExemploException() {
		super("");
	}
}