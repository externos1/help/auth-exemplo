package com.portoitapoa.authexemplo.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Fernando de Lima
 *
 */
@ResponseStatus(code=HttpStatus.NOT_FOUND, reason="Configurações não encontrada")
public class ConfiguracoesNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ConfiguracoesNotFoundException(String mensagem) {
		super(mensagem);
	}

	public ConfiguracoesNotFoundException() {
		super("Configurações não encontrada");
	}
}