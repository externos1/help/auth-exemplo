package com.portoitapoa.authexemplo.docs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Fernando de Lima
 *
 */
@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class BaseSwaggerConfig {
    
    @Value("${info.app.version}")
	private String appVersion;
	
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
        		.ignoredParameterTypes(Pageable.class, Sort.class)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.portoitapoa.authexemplo.endpoint.controller"))
                .build()
                .apiInfo(metaData());
    }

    private ApiInfo metaData() {
        return new ApiInfoBuilder()
        		.title("Auth-Exemplo API")
		        .description("API de Serviços, com exemplo para autenticação")
		        .license("")
		        .licenseUrl("http://unlicense.org")
		        .termsOfServiceUrl("http://teste.portoitapoa.com.br/auth-exemplo/v1/suporte")
		        .version(appVersion)
		        .contact(new Contact("Fernando de Lima","", "suporte@portoitapoa.com"))
		        .build();
    }
}