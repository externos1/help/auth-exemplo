package com.portoitapoa.authexemplo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

import com.portoitapoa.authexemplo.property.JwtConfiguration;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Fernando de Lima
 *
 */
@Slf4j
//@EnableEurekaClient
@EnableConfigurationProperties(value=JwtConfiguration.class)
@ComponentScan("com.portoitapoa")
@SpringBootApplication
@EnableGlobalMethodSecurity(
		  prePostEnabled = true, 
		  securedEnabled = true, 
		  jsr250Enabled = true)
public class AuthExemploApplication {

	@Value("${message}")
	private String message;

	public static void main(String[] args) {
		SpringApplication.run(AuthExemploApplication.class, args);
	}
	
	@Bean
	CommandLineRunner init() {
		return args -> {
			log.info(message);
		};
	}
}
